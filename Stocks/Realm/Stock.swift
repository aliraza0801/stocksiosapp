//
//  Stock.swift
//  Stocks
//
//  Created by Nauman Javed on 01/05/2019.
//  Copyright © 2019 cinnova. All rights reserved.
//
import Foundation
import Realm
import RealmSwift

class Stock: Object
{
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var firtPrice: Double = 0.0
    @objc dynamic var lastPrice: Double = 0.0
    @objc dynamic var image: Data?
    @objc dynamic var Itemdescription = ""
    @objc dynamic var weight: String = ""
    @objc dynamic var company = ""
    @objc dynamic var warrenty = ""
    @objc dynamic var color = ""
    @objc dynamic var categoryId = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    //Incrementa ID
    func IncrementaID() -> Int
    {
        let realm = try! Realm()
        if let retNext = realm.objects(Stock.self).sorted(byKeyPath: "id").last?.id
        {
            return retNext + 1
        }
        else
        {
            return 1
        }
    }
}
