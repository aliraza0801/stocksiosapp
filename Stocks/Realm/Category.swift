//
//  Category.swift
//  Stocks
//
//  Created by Nauman Javed on 02/05/2019.
//  Copyright © 2019 cinnova. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

class Category: Object
{
    @objc dynamic var id = 0
    @objc dynamic var categoryId = 0
    @objc dynamic var categoryName = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    //Incrementa ID
    func IncrementaID() -> Int
    {
        let realm = try! Realm()
        if let retNext = realm.objects(Category.self).sorted(byKeyPath: "id").first?.id {
            return retNext + 1
        }else{
            return 1
        }
    }
}
