//
//  FirstViewController.swift
//  Stocks
//
//  Created by Nauman Javed on 01/05/2019.
//  Copyright © 2019 cinnova. All rights reserved.
//
import RealmSwift
import DropDown
import UIKit
import MaterialComponents.MaterialBottomNavigation
extension UIViewController
{
    func hideKeyboardWhenTappedAround()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
}
enum ImageSource {
    case photoLibrary
    case camera
}
class FirstViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate
{

    @IBOutlet weak var btnChooseCategory: UIButton!
    @IBOutlet weak var tfCompany: UITextField!
    @IBOutlet weak var tfColor: UITextField!
    @IBOutlet weak var tfWarrenty: UITextField!
    @IBOutlet weak var tfWeight: UITextField!
    @IBOutlet weak var tfDescription: UITextField!
    @IBOutlet weak var tfFinalPrice: UITextField!
    @IBOutlet weak var tfFirstPrice: UITextField!
    @IBOutlet weak var tfItemName: UITextField!
    
    var pickedImage: UIImage!
    var imagePicker: UIImagePickerController!
    let chooseCategoryDropDown = DropDown()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tfCompany.attributedPlaceholder = NSAttributedString(string: "Company",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfColor.attributedPlaceholder = NSAttributedString(string: "Color",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfWarrenty.attributedPlaceholder = NSAttributedString(string: "Warrenty",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfWeight.attributedPlaceholder = NSAttributedString(string: "Weight",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfDescription.attributedPlaceholder = NSAttributedString(string: "Description",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfFinalPrice.attributedPlaceholder = NSAttributedString(string: "Final Price",
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfFirstPrice.attributedPlaceholder = NSAttributedString(string: "First Price",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        tfItemName.attributedPlaceholder = NSAttributedString(string: "Item Name",
                                                                attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        chooseCategoryDropDown.anchorView = btnChooseCategory
        chooseCategoryDropDown.bottomOffset = CGPoint(x: 0, y: btnChooseCategory.bounds.height)
        chooseCategoryDropDown.dataSource = [
            "Chairs", "Charpai","Bed","Rassi","Matress"
        ]
        
        chooseCategoryDropDown.selectionAction = { [weak self] (index, item) in
            self?.btnChooseCategory.setTitle(item, for: .normal)
        }
        
        
          self.hideKeyboardWhenTappedAround()
    }

    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    @IBAction func btnChooseCategoryClicked(_ sender: Any)
    {
        chooseCategoryDropDown.show()
    }
    
    @IBAction func btnAttachPicture(_ sender: Any)
    {
        guard UIImagePickerController.isSourceTypeAvailable(.camera) else {
            selectImageFrom(.photoLibrary)
            return
        }
        selectImageFrom(.camera)
    }
    
    func selectImageFrom(_ source: ImageSource){
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        switch source {
        case .camera:
            imagePicker.sourceType = .camera
        case .photoLibrary:
            imagePicker.sourceType = .photoLibrary
        }
        present(imagePicker, animated: true, completion: nil)
    }
    
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
     {
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else
        {
            print("Image not found!")
            return
        }
        pickedImage = selectedImage
    }
    
    func ConvertImageToBase64Format(image: UIImage!) -> String
    {
        let imageData:NSData = image.pngData()! as NSData
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        return strBase64
    }
    
    @IBAction func btnAddItem(_ sender: Any)
    {
        
            if(pickedImage == nil)
            {
                let alert = UIAlertController(title: "Missing Image", message: "Please attach an image before adding an Item.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                let realm = try! Realm()
                try! realm.write
                {
                var imageString: String = ConvertImageToBase64Format(image: pickedImage)
                let item = Stock()
                item.id = item.IncrementaID()
                item.name = tfItemName.text!
                item.firtPrice = Double(tfFirstPrice.text!)!
                item.lastPrice = Double(tfFinalPrice.text!)!
                item.image = pickedImage.data()
                item.Itemdescription = tfDescription.text!
                item.weight = tfWeight.text!
                item.company = tfCompany.text!
                item.warrenty = tfWarrenty.text!
                item.color = tfColor.text!
                
                realm.add(item)
                }
            
            
          
            }
      
    }
    
  
    
   
  
    
}

