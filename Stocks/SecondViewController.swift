//
//  SecondViewController.swift
//  Stocks
//
//  Created by Nauman Javed on 01/05/2019.
//  Copyright © 2019 cinnova. All rights reserved.
//
import RealmSwift
import UIKit
import DropDown
class SecondViewController: UIViewController,UITableViewDataSource, UITableViewDelegate
{
    private var data: [Stock] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
       return data.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
//        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//        let mainViewController = storyBoard.instantiateViewController(withIdentifier: "DetailsController") as? DetailsViewController
//        mainViewController?.stock = data[indexPath.row]
//        self.tabBarController?.present(mainViewController!, animated: true, completion: nil)
      // self.tabBarController?.navigationController?.pushViewController(mainViewController!, animated: true)
        
        
        if let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailsController") as? DetailsViewController {
            if let navigator = navigationController {
                viewController.hidesBottomBarWhenPushed = true
                viewController.stock = data[indexPath.row]
                navigator.pushViewController(viewController, animated: true)
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
    {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, handler) in
           // self.DeleteItem(_stock: self.data[indexPath.row])
            
        }
        deleteAction.backgroundColor = .red
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        return configuration
    }
    
    func DeleteItem(_stock: Stock)
    {
        let realm = try! Realm()
        try! realm.write {
            realm.delete(_stock)
        }
         self.data =  self.LoadStocksFromRealm()
        self.tblViewStocks.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tblViewStocks.dequeueReusableCell(withIdentifier: "StockCell") as? StockCell
        cell?.itemCost.text = "Rs." + String(data[indexPath.row].firtPrice) + " /-"
        cell?.itemDescription.text = String(data[indexPath.row].Itemdescription)
        cell?.itemName.text = data[indexPath.row].name
        cell?.itemImage.image = ConvertDataToImage(imageData:data[indexPath.row].image)
        
        return cell!
    }
    

   
     
    @IBOutlet weak var tblViewStocks: UITableView!
    @IBOutlet weak var SearchBar: UISearchBar!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.registerTableViewCells()
        data =  self.LoadStocksFromRealm()
        tblViewStocks.delegate = self
        tblViewStocks.dataSource = self
        
        self.navigationController?.navigationBar.isHidden = true

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        data =  self.LoadStocksFromRealm()
        tblViewStocks.reloadData()
        self.navigationController?.navigationBar.isHidden = true
    }
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
     {
        return 100
     }
    
    func registerTableViewCells()
    {
     let cell = UINib(nibName: "StockCell", bundle: nil)
     self.tblViewStocks.register(cell, forCellReuseIdentifier: "StockCell")
    }
    
    func LoadStocksFromRealm() -> [Stock]
    {
        let stocks = try! Realm().objects(Stock.self)
        let stocksArray: [Stock] = Array(stocks)
        return stocksArray
    }
    
    func ConvertDataToImage(imageData: Data?) -> UIImage
    {
        return UIImage.init(data: imageData!, scale: 1)!
    }
    
}

