//
//  DetailsViewController.swift
//  Stocks
//
//  Created by Nauman Javed on 23/05/2019.
//  Copyright © 2019 cinnova. All rights reserved.
//
import UIKit
import Foundation
import DTPhotoViewerController
class DetailsViewController: UIViewController
{
    var imageReceived: UIImage?
    var stock: Stock?
    

    
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var firstPrice: UILabel!
    @IBOutlet weak var finalPrice: UILabel!
    @IBOutlet weak var company: UILabel!
    @IBOutlet weak var weight: UILabel!
    @IBOutlet weak var warrenty: UILabel!
    @IBOutlet weak var color: UILabel!
    @IBOutlet weak var imgItem: UIImageView!

    
    
    override func viewWillAppear(_ animated: Bool)
    {
        PopulateValues()
        
       
    }
   
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
       
        
        var image: UIImage = ConvertDataToImage(imageData: stock?.image)
        imgItem.image = image
        imageReceived = ConvertDataToImage(imageData: stock?.image)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imgItem.isUserInteractionEnabled = true
        imgItem.addGestureRecognizer(tapGestureRecognizer)

       PopulateValues()
       
        
    }
    
    @IBAction func btnBackTouchUpInside(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func PopulateValues()
    {
        itemName.text = stock?.name
        itemDescription.text = stock?.Itemdescription
        itemDescription.sizeToFit()
        if let fp = stock?.firtPrice
        {
            let fpInt = Int(fp)
            firstPrice.text = "Rs. " + String(fpInt)
        }

        if let lp = stock?.lastPrice
        {
            let lpInt = Int(lp)
            finalPrice.text = "Rs. " + String(lpInt)
        }

        company.text = stock?.company
        weight.text = stock?.weight
        warrenty.text = stock?.warrenty
        color.text = stock?.color
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
         let viewController = DTPhotoViewerController(referencedView: imgItem, image: imageReceived!)
        
         self.present(viewController, animated: true, completion: nil)
    }
    
    
    func ConvertDataToImage(imageData: Data?) -> UIImage
    {
        return UIImage.init(data: imageData!, scale: 1)!
    }
    

}
