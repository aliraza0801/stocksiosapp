//
//  StockCell.swift
//  Stocks
//
//  Created by Nauman Javed on 21/05/2019.
//  Copyright © 2019 cinnova. All rights reserved.
//

import UIKit

class StockCell: UITableViewCell
{

    @IBOutlet weak var itemCost: UILabel!
    @IBOutlet weak var itemDescription: UILabel!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

    }
    
}
